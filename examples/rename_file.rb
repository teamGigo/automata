#!/usr/bin/env ruby
# frozen_string_literal: true

require "bundler/setup"
require "automata"

begin
  Automata::Rename.exec
rescue => e
  puts "#{e.class} found. #{e.message}"
end
