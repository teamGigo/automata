# Automata(Experiment)

Automata offers an abstraction layer for Linux administration. Commands may be shell scripts or ruby code. The intention is to
simplify administration tasks by taking only required information. Sensible defaults are set in every service. You may even place conditions

The application will create .auto files anywhere it has performed an action.

## Installation
```ruby
gem 'automata'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install automata

## Usage
  (simple io)
  1. automata rename -d [directory] -p [pattern] [new pattern] <condition>
  2. automata move -d [directory] -p [pattern] [new pattern] <condition>
  3. automata open "README.txt" if "README.txt".exists?
  4. automata delete/shelf -d [directory] if size>100MB || disk_space<500MB

  (simple administration)
  5. automata check disk
  6. automata check disk & report <condition>
  7. automata report imminent_failure
  8. automata set imminent_failure <conditions>
  9. automata configure smtp
 10. automata configure ssh

  (Setup cron tasks)
  11. automata exec "automata" when time=0800
  12. automata watch <script> every "thursday"  or automata watch everytime
  13. automata scan <configurations>

## Operations/Commands(To be added)
rename, move, create, delete, shelf  - simple io
config ,import, watch, set, report, check,  - simple administration



## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/automata. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/automata/blob/main/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Automata project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/automata/blob/main/CODE_OF_CONDUCT.md).
