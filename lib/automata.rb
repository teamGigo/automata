# frozen_string_literal: true
require 'colorize'
require_relative "automata/defaults"
#require_relative "automata/sequence"
require_relative "automata/rename"
module Automata
  class Error < StandardError; end



  #Default configuration flags are checked on the arguments
  # e.g. help
  def Automata.configuration_flags

  end
  # Loads the arguments and assigns them to the correct variables
  # This method is called by all "actions" before their execution
  def Automata.validate_paramaters(flags,parameters)
    raise ArgumentError , "Application requires arguments".colorize(:red) if ARGV.length==0

    if ARGV.grep(flags)
      index=1
      ARGV.each do |param|
            case param
              when "-d"
                @folderpath = Dir.new(ARGV[index])
              when "-p"
                  @pattern = ARGV[index]
                  @new_pattern = ARGV[index+1]
              when "-h"
                  self.help
            end
          index+=1
      end

    else
      if Dir.exist?ARGV[0]
        @folderpath = Dir.new(ARGV[0])
      else
        raise ArgumentError , "Specify The folder path as the first Argument".colorize(:red)
      end

      if ARGV[1]!=nil && ARGV[2]

        @folderpath = Dir.new(ARGV[0])
        @pattern = ARGV[1]
        @new_pattern = ARGV[2]

      else
        raise ArgumentError , "You must specify the old & new patterns".colorize(:red)
      end

    end
      [@pattern, @new_pattern]
  end


end
