# frozen_string_literal: true

module Automata
  VERSION = "0.1.0"
  VERBOSE = false

  #Allows applications extending the library to add commands of their own
  def register_action
  end

  #Removes commands and/or extensions from being called by the library
  def uninstall_action
  end

  #Task that should run before a command is run
  # e.g. mapping flags and configurations
  def before_action
  end
  #tasks that should run after a command is run
  def after_action
  end

end
