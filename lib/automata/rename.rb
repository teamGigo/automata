# frozen_string_literal: true

module Automata

  class Rename

    VERSION=0.1

    def self.exec
      self.validate_paramaters
      affected_files=[]

      #Filter results of the files with issues
      @folderpath.each do |file|
        if file.match?(@pattern)
          affected_file = File.join(@folderpath,file)

          if File.exists?affected_file
            remaining_length = file.length - @pattern.length

            #get the words after the pattern
            replaced_name = @new_pattern+file[@pattern.length,remaining_length]
            new_file_name = File.join(@folderpath,replaced_name)
            print "Rename #{file} to #{replaced_name}?(Y/N)"
            response = $stdin.gets.chomp
            if response.downcase == 'y'
              affected_files << file
              File.rename(affected_file,new_file_name)
            end
          end
        end
      end
      @folderpath.close

      puts "#{affected_files.length} files changed"

    end

    private

    def self.flag_pattern
      %r(-d|-p|-h)
    end

    def self.validate_paramaters

      raise ArgumentError , "Application requires arguments".colorize(:background => :red) if ARGV.length==0
      if ARGV.grep(self.flag_pattern)
        index=1
        ARGV.each do |param|
          case param
            when "-d"
              @folderpath = Dir.new(ARGV[index])
            when "-p","-pattern","--patterns"
                @pattern = ARGV[index]
                raise ArgumentError , "Place the replacement pattern/literal after the current pattern".colorize(:background => :red) if ARGV[index+1]==nil
                @new_pattern = ARGV[index+1]
            when "-h","--help","-help"
                puts self.help
                exit()
            when "-v"
                @verbose = true
          end
          index+=1
        end
        #ensure folderpath is filled. This should be the default
        if @folderpath.nil?
          @folderpath = Dir.new(Dir.getwd)
          puts "On #{Dir.getwd}".colorize(:background => :red)
        end
      else
        if Dir.exist?ARGV[0]
          @folderpath = Dir.new(ARGV[0])
        else
          raise ArgumentError , "Specify The folder path as the first Argument".colorize(:background => :red)
        end

        if ARGV[1]!=nil && ARGV[2]

          @folderpath = Dir.new(ARGV[0])
          @pattern = ARGV[1]
          @new_pattern = ARGV[2]

        else
          raise ArgumentError , "You must specify the old & new patterns".colorize(:background => :red)
        end

      end

    end

    def self.keywords
      ["current_pattern","new_pattern","directory","file_size","file_extension",]
    end

    def self.help
        help_guide = ["\n \n \t","automata rename -d [Directory] -p [current_pattern] [new_pattern] if <condition>".colorize(:background => :red)]
        help_guide << "[Directory]".colorize(:background => :blue)+" is optional"
        help_guide << "Placing Conditions is optional. Triggered with an 'if' clause ".colorize(:background => :yellow)
        help_guide << "Placed Conditions can only check resources using the reserved Keywords below".colorize(:background => :yellow)
        help_guide << self.keywords.join(",").colorize(:green)
        help_guide
    end

  end

end
