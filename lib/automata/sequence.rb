# frozen_string_literal: true
# instance point

require_relative "./storage"

module Automata
  class Sequence
      # creates, reads or updates an autofile
      # This class method returns a sequence instance within its folder structure and references the variable location of its parent and children .auto instances
      attr_accessor :database

      def self.create (param)
        if param.length==0
          raise ArgumentError, "Specify folder to sequence"
          exit()
        end
        #read folder
        if Dir.exist?param[0]
            puts "Directory: #{param[0]} "
            @folder = Dir.new(param[0])
            if @folder.children.length>0
              puts "--------------------"
              puts "Navigate Files with number"
              puts "--------------------"
              @folder.children.each_with_index  do |child,index|
                  print "(#{index}) #{child} \t"
              end

              if param[1]
                  puts "reading file"
                  #current_file = @folder.children[param[1].to_i ]

                  exec "#{param[1]} #{param[2]}"
              end
            end

        else
            raise IOError , "Folder does not exist"
        end

        # loop through files
        # read file at any time
        # perform any action you want on a file
      end

      def move(action)
          if action == "prev"
             prev_step
          else
            next_step
          end
      end

      private

      def next_step
      end

      def prev_step
      end

  end
end
