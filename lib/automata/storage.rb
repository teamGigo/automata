# frozen_string_literal: true
module Automata
  module Storage
    #--
    # instance
    #++
    class History
      attr_accessor :folder_name,:files,:last_updated,:created_at
      attr_accessor :users

    end
  end
end
