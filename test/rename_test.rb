# frozen_string_literal: true

require "test_helper"

class RenameTest < Minitest::Test

  def test_it_has_version
    refute_nil ::Automata::Rename::VERSION
  end
  def test_it_has_flags_pattern
    refute_nil ::Automata::Rename.flag_pattern
  end

  def test_it_has_documentation
      refute_nil ::Automata::Rename.help
  end
  def test_it_supports_keywords
    refute_empty ::Automata::Rename.keywords
  end
  #
  # def test_it_supports_undo_action
  #     refute_nil ::Automata::Rename.undo
  # end
  #
  # def test_it_has_exec
  #   refute_nil ::Automata::Rename.exec
  # end
end
